﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    public class City
    {
        /// <summary>
        /// City name in Russian
        /// </summary>
        public string Name_ru { get; set; }

        /// <summary>
        /// City name in english
        /// </summary>
        public string Name_en { get; set; }

        
        public static implicit operator City(Dictionary<string, object> cityKeyValues)
        {
            var city = new City();

            if (cityKeyValues.TryGetValue("names", out object cityNamesObj))
            {
                var cityNamesKeyValues = (IDictionary<string, object>)cityNamesObj;

                if (cityNamesKeyValues.TryGetValue("en", out object name_en))
                    city.Name_en = name_en as string;

                if (cityNamesKeyValues.TryGetValue("ru", out object name_ru))
                    city.Name_ru = name_ru as string;
            }
            else
                throw new ArgumentException("cityKeyValyes doesnt has names key");

            return city;
        }
    }

}
