﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string TimeZone { get; set; }

        public static implicit operator Location(Dictionary<string, object> locationKeyValues)
        {
            if (locationKeyValues is null)
                throw new ArgumentNullException("LocationKeyValues parameter is null");

            var location = new Location();

            location.Latitude = locationKeyValues.TryGetValue("latitude", out object latitude)
                                ? (double)latitude
                                : Throw<double>("latitude");

            location.Longitude = locationKeyValues.TryGetValue("longitude", out object longitude)
                                 ? (double)longitude
                                 : Throw<double>("longitude");

            location.TimeZone = locationKeyValues.TryGetValue("time_zone", out object timeZone)
                                ? (string)timeZone
                                : Throw<string>("time_zone");

            return location;
        }


        private static T Throw<T>(string key)
            => throw new ArgumentException($"lcationKeyValues doesn't contain {key} with typeof {nameof(T)}");

    }

}
