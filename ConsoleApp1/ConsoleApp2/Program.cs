﻿using ICSharpCode.SharpZipLib.Tar;
using MaxMind.Db;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Arrange necessary files/folders

            string currentDir = Directory.GetCurrentDirectory();

            // This gzip file is pulled from geolite server via Http
            string gzipCompressedFile = Path.Combine(currentDir, "geolite.tar.gz");

            // Gzip file will be decompressed to this tar file
            var tarArchiveFile = Path.Combine(currentDir, "geolite.tar");

            // Tar file will be extracted to this directory
            var geoliteFolder = Path.Combine(currentDir, "geolite");

            // Database file that we take from folder and save in our working Directory
            var geoliteDB = Path.Combine(currentDir, "geolite.mmdb");

            // What if directory exists ?
            Directory.CreateDirectory(geoliteFolder);
            #endregion

            goto MainLine;

            #region Pull geolite.tar.gz gzip file to working directory
            // this works
            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync("https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=BZkpEWQK0BQFArCf&suffix=tar.gz")
                                         .Result;

                using var stream = response.Content.ReadAsStreamAsync().Result;
                using var fileStream = File.Create(gzipCompressedFile);
                {
                    if (stream.CanRead)
                    {
                        byte[] buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        fileStream.Write(buffer, 0, buffer.Length);
                    }
                }

            }

        #endregion

            #region Decompress geolite.tar.gz to getolite.tar file 


            using (var compressedFileStream = new FileStream(path: gzipCompressedFile, FileMode.Open))
            using (var decompressedFileStream = new FileStream(path: tarArchiveFile, FileMode.Create))
            using (var gzipStream = new GZipStream(compressedFileStream, CompressionMode.Decompress))
            {
                gzipStream.CopyTo(decompressedFileStream);


            }

        #endregion

            #region Extract tar file to folder

            using (var decompressedFileStream = new FileStream(path: tarArchiveFile, FileMode.Open))
            using (var tarStream = TarArchive.CreateInputTarArchive(decompressedFileStream))
                tarStream.ExtractContents(geoliteFolder);


            #endregion

            #region Get mmdb from folder and paste it in working directory

            var directory = new DirectoryInfo(geoliteFolder);
            var mmdbFiles = directory.GetFiles("*.mmdb", SearchOption.AllDirectories);

            if (mmdbFiles.Length > 1)
                throw new FileLoadException($"More that one .mmdb exist exist in {geoliteFolder}");


            FileInfo geoliteDatabase = mmdbFiles.FirstOrDefault();

            if (geoliteDatabase is null)
                throw new FileLoadException($"No file of type .mmdb has been found {geoliteFolder}");

            geoliteDatabase.MoveTo(geoliteDB);

            #endregion

            #region Clean unnecessary  files/folders

            File.Delete(gzipCompressedFile);
            File.Delete(tarArchiveFile);
            Directory.Delete(geoliteDB);

            #endregion

            MainLine:

            using (var reader = new Reader(geoliteDB))
            {
                //var responses2 = reader.FindAll<object>();

                //foreach (var item in responses2)
                //{
                //    Console.WriteLine(item.Start);
                //    //Thread.Sleep(500);
                //}

                var responses  = reader.FindAll<Response>();

                foreach (var item in responses)
                {
                    Console.WriteLine(item.Start);
                    //Thread.Sleep(500);
                }

                Console.Read();
            }
        }
    }
}
