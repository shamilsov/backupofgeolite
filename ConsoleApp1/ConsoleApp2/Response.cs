﻿using MaxMind.Db;
using System.Collections.Generic;

namespace ConsoleApp2
{
    public class Response
    {
        public City City { get; set; }
        public Country Country { get; set; }
        public Location Location { get; set; }

        [Constructor]
        public Response([Parameter("city")] Dictionary<string,object> city,
                        [Parameter("country")] Dictionary<string, object> country,
                        [Parameter("location")] Dictionary<string, object> location)
        {
            City = city ?? default(City);
            Country = country ?? default(Country);
            Location = location ?? default(Location);
        }
    }
}
